select Country , count(CustomerID)
from Customers
group by Country ;

select ShipperName, count(OrderID) as count_OrderID
from Orders join Shippers on Orders.ShipperID = Shippers.ShipperID
group by ShipperName 
order by count_OrderID desc ;